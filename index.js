'use strict'

const http = require('http')
const request = require('request')
const fs = require('fs')

const port = 8081

let server = http.createServer((req, resp) => {

  const path = req.url
  console.log('PATHH', path)

  if (!path.match(/\/load-url/) && path != '/') {
    resp.writeHead(200, {'Content-Type': 'application/json'})
    resp.write('{"error": "Invalid url"}')
    return resp.end()
  }

  if (path == '/') {
    const htmlFile = fs.readFileSync('./antenna-explorer.html')
    resp.writeHead(200, {'Content-Type': 'text/html'})
    resp.write(htmlFile.toString())
    resp.end()
  } else {
    const params = path.split('?')[1]
    let ip = params.replace('url=', '')
    ip = ip.match(/^(http|https)/) ? ip : 'http://' + ip

    request.get(ip, (error, response, body) => {
      resp.writeHead(response.statusCode, response.headers)
      resp.write(body)
      resp.end()
    })

  }
})

server.listen(port);
console.log('Server is listening at:', port);